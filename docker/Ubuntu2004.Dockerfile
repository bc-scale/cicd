# Copyright 2020-2022 Zinchenko Serhii <zinchenko.serhii@pm.me>.
#
# Cpp Template Project: A template CMake project to get you started with
# C++ and tooling.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

FROM ubuntu:20.04

WORKDIR /tmp

# Setup system and install packages

COPY scripts/os/setup_ubuntu2004.sh /tmp/
COPY requirements.txt /tmp/
RUN cd /tmp && bash setup_ubuntu2004.sh && rm -rf ./*

# Fetch Conan dependencies

COPY scripts/fetch_conan_dependencies.sh /tmp/
COPY cmake/Conan.cmake /tmp/CMakeLists.txt
RUN cd /tmp && bash fetch_conan_dependencies.sh && rm -rf ./*

# Remove all temporary files

RUN rm -rf /tmp/*

# Install CI/CD scripts and linters config files

COPY scripts/os/install_files.sh /tmp/
COPY .* scripts/cicd/*.* scripts/cicd/*/* /tmp/
RUN bash /tmp/install_files.sh && rm -rf /tmp/*

ENV BASH_ENV "/etc/profile"

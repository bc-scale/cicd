# Copyright 2020-2022 Zinchenko Serhii <zinchenko.serhii@pm.me>.
#
# Cpp Template Project: A template CMake project to get you started with
# C++ and tooling.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Never try to install CentOS 7 SCL packages and build with them something in a
# same Docker file. This is just a one big pain. But if you still want...
#
# First of all I tried a below solution, that is based on:
#   - https://stackoverflow.com/questions/20635472
#   - https://stackoverflow.com/questions/55901985
#
# CentOS7.Dockerfile
#   ...
#   RUN echo "source scl_source enable devtoolset-8" >> /etc/bashrc && \
#       echo "source scl_source enable llvm-toolset-7.0" >> /etc/bashrc
#
#   Make SCL available globaly in user environment.
#   SHELL ["/bin/bash", "-c", "source /etc/bashrc"]
#   ...
#   # Make SCL available to anyone how use this docker image.
#   ENTRYPOINT ["/bin/bash", "-c", "source /etc/bashrc"]
#
# I don't know why but the above Docker file doesn't work at all. It literally
# breaks GitLab CI. CI doesn't run 'script:' section with a such docker and
# already report successful result.
#
# Working solution is based on https://github.com/sclorg/devtoolset-container/
# docker container for 7-toolchain.

FROM centos:7

# Setup system and install packages

COPY scripts/os/setup_centos7.sh /tmp/
COPY requirements.txt /tmp/
RUN cd /tmp && bash setup_centos7.sh && rm -rf ./*

# Enable the SCL for all bash scripts.
ENV BASH_ENV=/etc/profile

# Fetch Conan dependencies

COPY scripts/fetch_conan_dependencies.sh /tmp/
COPY cmake/Conan.cmake /tmp/CMakeLists.txt
RUN cd /tmp && bash fetch_conan_dependencies.sh && rm -rf ./*

# Remove all temporary files

RUN rm -rf /tmp/*

# Install CI/CD scripts and linters config files

COPY scripts/os/install_files.sh /tmp/
COPY .* scripts/cicd/*.* scripts/cicd/*/* /tmp/
RUN bash /tmp/install_files.sh && rm -rf /tmp/*

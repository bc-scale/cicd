################################
Common CI/CD Tools Documentation
################################

.. toctree::
   :maxdepth: 1
   :hidden:

   Developer Guides <https://cpptp.gitlab.io>

   cicd/index.rst

   CMake Guides <https://cpptp.gitlab.io/cmake/index.html>
   Docker Guides <https://cpptp.gitlab.io/docker/index.html>

   git/index.rst

   RST Guides <https://cpptp.gitlab.io/restructuredtext/index.html>
   Shell Guides <https://cpptp.gitlab.io/shell/index.html>

=========
Misc Jobs
=========

This is a list of almost all available misc jobs that are currently run in our CI/CD pipelines.

Stages overview
===============

Misc stage
----------

To avoid duplication of already known information please take a look at **misc stage** overview from :ref:`overview-misc-stage`.

.. _misc-jobs-remove-old-pipelines:

remove_old_pipelines
^^^^^^^^^^^^^^^^^^^^

This job executes a custom script that removes outdated pipelines from project pipeline list.

**ENVIRONMENT**

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity
      - Origin

    * - CI_API_V4_URL
      - Mandatory
      - GitLab

    * - CI_PROJECT_ID
      - Mandatory
      - GitLab

    * - CPPTP_CI_PRIVATE_API_TOKEN
      - Mandatory
      -

    * - CPPTP_CI_GENERIC_PACKAGES_REGISTRY
      - Mandatory
      -

**TRIGGERS**
    Jobs are triggered only by a scheduler.

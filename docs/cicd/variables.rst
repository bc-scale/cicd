=========
Variables
=========

Here are all project related CI/CD variables that are used inside our GitLab pipeline, along with a brief explanation.

Local CI/CD variables
=====================

**CI_DEBUG_TRACE**:STRING
  Predefined GitLab CI variable that enables `debug logging (tracing) <https://docs.gitlab.com/ee/ci/variables/index.html#debug-logging>`__ to help troubleshoot problems with pipeline configuration or job scripts.

  Valid values: ``false`` and ``true``.

  Defaults to ``false``.

  .. warning::
    Debug logging can be a serious security risk.
    The output contains the content of all variables and other secrets available to the job.
    The output is uploaded to the GitLab server and visible in job logs.

**CPPTP_CI_DOCS_NAME**:STRING
  A name of an archive with generated HTML documentation which will be deployed to the `artifacts <https://gitlab.com/cpptp/artifacts>`__ package registry.

  Usually defaults to one of ``CPPTP_CI_*_DOCS_NAME`` variables.

**CPPTP_CI_LINTER_TRACE**:STRING
  Enable debug logging to help troubleshoot problems with linter jobs.
  Debug logging exposes job execution details that are usually hidden and makes job logs more verbose.

  Valid values: ``false`` and ``true``.

  Defaults to ``false``.

**CPPTP_CI_MERGE_REQUEST_CHANGED_FILES**:LIST
  A space-separated list of all files that have been added or changed.
  This list doesn't contain deleted files and files from 3rd-party folder.

  .. note:: This variable is only available for **linter** jobs.

Global CI/CD variables
======================

**CPPTP_CI_CICD_DOCS_NAME**:STRING
  A package name for `CI/CD <https://gitlab.com/cpptp/cicd>`__ project HTML docs.

**CPPTP_CI_DEPLOY_TOKEN**:STRING
  A `deploy token <https://docs.gitlab.com/ee/user/project/deploy_tokens>`__ for `artifacts`_'s `packages and registries <https://docs.gitlab.com/ee/user/packages/>`__.

  **Scopes:** ``read_registry``, ``write_registry``, ``read_package_registry``, ``write_package_registry``.

**CPPTP_CI_DEPLOY_USER**:STRING
  A user for ``CPPTP_CI_DEPLOY_TOKEN``.

**CPPTP_CI_GENERIC_PACKAGES_REGISTRY**:URL
  A link to an `artifacts`_'s `generic packages repository <https://docs.gitlab.com/ee/user/packages/generic_packages>`__.

**CPPTP_CI_IMAGES_REGISTRY**:URL
  A link to an `artifacts`_'s `container registry <https://docs.gitlab.com/ee/user/packages/container_registry>`__.

**CPPTP_CI_PRIVATE_API_TOKEN**:STRING
  A `private token <https://docs.gitlab.com/ee/user/profile/personal_access_tokens>`__ of ``CPPTP_CI_USER`` with read-write access rights for API calls.

  **Scopes:** ``api``.

**CPPTP_CI_USER**:STRING
  An artificial user with maintainer rights.

.. _artifacts: https://gitlab.com/cpptp/artifacts

GitLab group-level variables
----------------------------

+----------+------------------------------------+----------------------------+-----------+---------+--------------+
| Type     | Key                                | Value                      | Protected | Masked  | Environments |
+==========+====================================+============================+===========+=========+==============+
| Variable | CPPTP_CI_CICD_DOCS_NAME            | cicd_docs                  | **yes**   | no      | All          |
+----------+------------------------------------+----------------------------+-----------+---------+--------------+
| Variable | CPPTP_CI_DEPLOY_TOKEN              | \-                         | no        | **yes** | All          |
+----------+------------------------------------+----------------------------+-----------+---------+--------------+
| Variable | CPPTP_CI_DEPLOY_USER               | gitlab\+deploy\-token\-{n} | no        | **yes** | All          |
+----------+------------------------------------+----------------------------+-----------+---------+--------------+
| Variable | CPPTP_CI_GENERIC_PACKAGES_REGISTRY | \-                         | **yes**   | no      | All          |
+----------+------------------------------------+----------------------------+-----------+---------+--------------+
| Variable | CPPTP_CI_IMAGES_REGISTRY           | \-                         | no        | no      | All          |
+----------+------------------------------------+----------------------------+-----------+---------+--------------+
| Variable | CPPTP_CI_PRIVATE_API_TOKEN         | \-                         | **yes**   | **yes** | All          |
+----------+------------------------------------+----------------------------+-----------+---------+--------------+
| Variable | CPPTP_CI_USER                      | \-                         | **yes**   | no      | All          |
+----------+------------------------------------+----------------------------+-----------+---------+--------------+

.. note::
  To change above environment variables go to `group CI/CD settings <https://gitlab.com/groups/cpptp/-/settings/ci_cd>`__ to *Variables* section.

Misc CI/CD variables
====================

**CPPTP_CI_ENABLE_PIPELINES_CLEANUP**:STRING
  Enable :ref:`misc-jobs-remove-old-pipelines` CI job in pipeline.
  This variable is typically used only on scheduled pipelines to enable only this specific job.

  Valid values: ``false`` and ``true``.

  Defaults to ``false``.

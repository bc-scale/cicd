================
Linters Pipeline
================

This is a pipeline for running different linters on all available type of files to check them for consistency of all our code conventions, and to detect possible issues as fast as it's possible.

Flowchart
=========

This is a very generalized flowchart that combines in itself all possible roots of pipeline generation and execution.

..
  Flowchart has been generated with the help of https://app.diagrams.net/.
  If you need to change flowchart:
    1. Go to the mentioned site.
    2. Import _static/cicd/linters_pipeline_flowchart.drawio.
    3. Change flowchart
    4. Save as .drawio and .jpg files.
    5. Update appropriate files in _static folder.

.. image:: /_static/cicd/linters_pipeline_flowchart.jpg
   :target: ../_images/cicd/linters_pipeline_flowchart.jpg
   :alt: Linters Pipeline Flowchart

Stages overview
===============

Check stage
-----------

This is a stage for running different linters on appropriate set of modified files.

To avoid duplication of already known information please take a look at **check stage** overview from* :ref:`overview-check-stage`.

However to avoid going back and force some of the most important information still will be put here.

All jobs from this stage are triggered only on **merge requests** and if a certain part of the projects have been modified (this can be C++, Docker, Shell files and so on...).

All jobs are not allowed to fail as all project code must follow established conventions and contain no know defects and issues.

check:cmake:format
^^^^^^^^^^^^^^^^^^

This job executes `cmake-format <https://github.com/cheshirekow/cmake_format>`__ *only on changed CMake files* (not diff, but files) which will reformat code accordingly to the established project code style.

Configuration file is located at the root of the repository and has ``.cmake-format.py`` name.

**ALLOW FAILURE**
    .. warning:: This job is allowed to fail as it's still in development.

**ARTIFACTS**
    Job will generate a patch file with corrected lines.

**ENVIRONMENT**

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity

    * - CPPTP_CI_LINTER_TRACE
      - Optional

    * - CPPTP_CI_MERGE_REQUEST_CHANGED_FILES
      - Mandatory

**TRIGGERS**
    Job will be triggered only on **merge requests** and only if there were changes in the below files:

    - ``*/CMakeLists.txt``,
    - ``**/*.cmake``.

check:cmake:lint
^^^^^^^^^^^^^^^^

This job executes `cmake-lint <https://github.com/cheshirekow/cmake_format>`__ *only on changed CMake files* (not diff, but files) which will diagnose and fix style violations like wrong variable and function names.

Configuration file is located at the root of the repository and has ``.cmake-format.py`` name.

**ALLOW FAILURE**
    .. warning:: This job is allowed to fail as it's still in development.

**ARTIFACTS**
    Job will generate a patch file with corrected lines.

**ENVIRONMENT**

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity

    * - CPPTP_CI_LINTER_TRACE
      - Optional

    * - CPPTP_CI_MERGE_REQUEST_CHANGED_FILES
      - Mandatory

**TRIGGERS**
    Job will be triggered only on **merge requests** and only if there were changes in the below files:

    - ``*/CMakeLists.txt``,
    - ``**/*.cmake``.

check:code:license
^^^^^^^^^^^^^^^^^^

This job executes `licenseheaders <https://github.com/johann-petrak/licenseheaders>`__ on project source code files, which will detect incorrect license headers.

Configuration file is located at the root of the repository and has ``.license.tmpl`` name.

**ALLOW FAILURE**
    .. warning:: This job is allowed to fail as it's still in development.

**ARTIFACTS**
    Job will generate a patch file with corrected lines.

**ENVIRONMENT**

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity

    * - CPPTP_CI_LINTER_TRACE
      - Optional

    * - CPPTP_CI_MERGE_REQUEST_CHANGED_FILES
      - Mandatory

**TRIGGERS**
    Job will be triggered only on **merge requests** and on all project files except ``3rd-party`` folder.

check:code:spelling
^^^^^^^^^^^^^^^^^^^

This job executes `codespell <https://github.com/codespell-project/codespell>`__ on project source code files, which will detect common misspellings.

Configuration file is located at the root of the repository and has ``.codespellrc`` name.

**ARTIFACTS**
    Job will generate a simple log file with all found issues.

**ENVIRONMENT**

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity

    * - CPPTP_CI_LINTER_TRACE
      - Optional

    * - CPPTP_CI_MERGE_REQUEST_CHANGED_FILES
      - Mandatory

**TRIGGERS**
    Job will be triggered only on **merge requests** and on all project files except ``3rd-party`` folder.

check:commits:spelling
^^^^^^^^^^^^^^^^^^^^^^

This job executes `codespell <https://github.com/codespell-project/codespell>`__ on all found branch commits, which will detect common misspellings.

Configuration file is located at the root of the repository and has ``.codespellrc`` name.

**ARTIFACTS**
    Job will generate a simple log file with all found issues.

**ENVIRONMENT**

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity
      - Origin

    * - CI_MERGE_REQUEST_TARGET_BRANCH_NAME
      - Mandatory
      - GitLab

**TRIGGERS**
    Job will be triggered only on **merge requests**.

.. _linters-pipeline-check-cpp-coverage:

check:cpp:coverage
^^^^^^^^^^^^^^^^^^

This job calculates an overall coverage of C++ source code by tests.
In order to do that job builds project with compiler specific flags which allow to generate files that can be processed by `Gcov <https://en.wikipedia.org/wiki/Gcov>`__.

At this moment job doesn't reuse artifacts from *build* or *test* stage.
Even so it means pipeline could take much less time, it still much easy to manage a job by itself.

**ARTIFACTS**
    Job will generate a set of files suitable for generating an HTML report.

**ENVIRONMENT**

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity

    * - CPPTP_CI_CMAKE_GLOBAL_ARGS
      - Mandatory

**TRIGGERS**
    Job will be triggered only on **merge requests** and only if there were changes in the below files:

    - ``project/**/*.cpp``,
    - ``project/**/*.hpp``,
    - ``project/**/*.ipp``.

check:cpp:format
^^^^^^^^^^^^^^^^

This job executes `clang-format <https://clang.llvm.org/docs/ClangFormat.html>`__ *only on changes from C++ files* which will reformat code accordingly to the established project code style.

``clang-format`` for each input file will try to find the ``..clang-format`` file located in the closest parent directory of the input file.

Different configuration options can be found on `official page <https://clang.llvm.org/docs/ClangFormatStyleOptions.html>`__

**ALLOW FAILURE**
    .. warning:: This job is allowed to fail as it's still in development.

**ARTIFACTS**
    Job will generate a patch file with corrected lines.

**ENVIRONMENT**

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity
      - Origin

    * - CI_COMMIT_SHA
      - Mandatory
      - GitLab

    * - CI_MERGE_REQUEST_TARGET_BRANCH_NAME
      - Mandatory
      - GitLab

    * - CPPTP_CI_LINTER_TRACE
      - Optional
      -

    * - CPPTP_CI_MERGE_REQUEST_CHANGED_FILES
      - Mandatory
      -

**TRIGGERS**
    Job will be triggered only on **merge requests** and only if there were changes in the below files:

    - ``project/**/*.cpp``,
    - ``project/**/*.hpp``,
    - ``project/**/*.ipp``.

check:cpp:include_guards
^^^^^^^^^^^^^^^^^^^^^^^^

This job executes ``checkguard`` from `guardonce <https://github.com/cgmb/guardonce>`__ package *only on changed C++ files* (not diff, but files) which will detect wrong written `include guards <https://en.wikipedia.org/wiki/Include_guard>`__.

**ARTIFACTS**
    Job will generate a simple log file with all found issues.

**ENVIRONMENT**

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity
      - Origin

    * - CI_PROJECT_DIR
      - Mandatory
      - GitLab

    * - CPPTP_CI_LINTER_TRACE
      - Optional
      -

    * - CPPTP_CI_MERGE_REQUEST_CHANGED_FILES
      - Mandatory
      -

**TRIGGERS**
    Job will be triggered only on **merge requests** and only if there were changes in the below files:

    - ``project/**/*.cpp``,
    - ``project/**/*.hpp``,
    - ``project/**/*.ipp``.

.. _linters-pipeline-check-cpp-cppcheck:

check:cpp:cppcheck
^^^^^^^^^^^^^^^^^^

This job executes `cppcheck <https://github.com/danmar/cppcheck>`__ *only on changed C++ files* (not diff, but files) which detect bugs and focuses on detecting undefined behavior and dangerous coding constructs.

Configuration file is located at the root of the repository and has ``.cppcheck`` name.

**ARTIFACTS**
    Job will generate a set of files suitable for generating an HTML report.

**ENVIRONMENT**

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity

    * - CPPTP_CI_CMAKE_GLOBAL_ARGS
      - Mandatory

**TRIGGERS**
    Job will be triggered only on **merge requests** and only if there were changes in the below files:

    - ``project/**/*.cpp``,
    - ``project/**/*.hpp``,
    - ``project/**/*.ipp``.

.. _linters-pipeline-check-cpp-tidy:

check:cpp:tidy
^^^^^^^^^^^^^^

This job executes `clang-tidy <https://clang.llvm.org/extra/clang-tidy>`__ *only on a diff* which will diagnose and fix typical programming errors, like style violations, interface misuse, or bugs that can be deduced via static analysis.

Configuration file is located at the root of the repository and has ``.clang-tidy`` name.

**ARTIFACTS**
    Job will generate a simple log file with all found issues.

**ENVIRONMENT**

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity
      - Origin

    * - CI_MERGE_REQUEST_TARGET_BRANCH_NAME
      - Mandatory
      - GitLab

    * - CPPTP_CI_LINTER_TRACE
      - Optional
      -

    * - CPPTP_CI_MERGE_REQUEST_CHANGED_FILES
      - Mandatory
      -

**TRIGGERS**
    Job will be triggered only on **merge requests** and only if there were changes in the below files:

    - ``project/**/*.cpp``,
    - ``project/**/*.hpp``,
    - ``project/**/*.ipp``.

check:cpp:tidy (nightly)
^^^^^^^^^^^^^^^^^^^^^^^^

This is almost the same job as :ref:`linters-pipeline-check-cpp-tidy` with one slight difference.
This is a nightly job which checks entire code base on a daily bases.

The main reason for such separation is ability to detect a set of issues that is impossible to find while only checking files diff.
The downside of this solution is a long running job, which can take more than half an hour to run.

check:docker:lint
^^^^^^^^^^^^^^^^^

This job executes `hadolint <https://github.com/hadolint/hadolint>`__ *only on changed Docker files* (not diff, but files) which will diagnose bugs that can be deduced via static analysis.

Configuration file is located at the root of the repository and has ``.hadolint.yaml`` name.

**ARTIFACTS**
    Job will generate a simple log file with all found issues.

**ENVIRONMENT**

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity

    * - CPPTP_CI_LINTER_TRACE
      - Optional

    * - CPPTP_CI_MERGE_REQUEST_CHANGED_FILES
      - Mandatory

**TRIGGERS**
    Job will be triggered only on **merge requests** and only if there were changes in the below files:

    - ``**/.Dockerfile``.

check:python:black
^^^^^^^^^^^^^^^^^^

This job executes `black <https://github.com/psf/black>`__ *only on changed python files* (not diff, but files) which will diagnose and fix style violations.

**ARTIFACTS**
    Job will generate a patch file with corrected lines.

**ENVIRONMENT**

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity

    * - CPPTP_CI_LINTER_TRACE
      - Optional

    * - CPPTP_CI_MERGE_REQUEST_CHANGED_FILES
      - Mandatory

**TRIGGERS**
    Job will be triggered only on **merge requests** and only if there were changes in the below files:

    - ``**/*.py``.

check:python:flake8
^^^^^^^^^^^^^^^^^^^

This job executes `flake8 <https://github.com/PyCQA/flake8>`__ *only on changed python files* (not diff, but files) which will diagnose and fix style violations, or bugs that can be deduced via static analysis.

Configuration file is located at the root of the repository and has ``.flake8`` name.

**ARTIFACTS**
    Job will generate a simple log file with all found issues.

**ENVIRONMENT**

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity

    * - CPPTP_CI_LINTER_TRACE
      - Optional

    * - CPPTP_CI_MERGE_REQUEST_CHANGED_FILES
      - Mandatory

**TRIGGERS**
    Job will be triggered only on **merge requests** and only if there were changes in the below files:

    - ``**/*.py``.

check:python:isort
^^^^^^^^^^^^^^^^^^

This job executes `isort <https://pycqa.github.io/isort>`__ *only on changed python files* (not diff, but files) which will diagnose and fix issues related to python imports.

``isort`` for each input file will try to find the ``.isort.cfg`` file located in the closest parent directory of the input file.

Different configuration options can be found on `official page <https://pycqa.github.io/isort/docs/configuration/options.html>`__

**ARTIFACTS**
    Job will generate a patch file with corrected lines.

**ENVIRONMENT**

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity

    * - CPPTP_CI_LINTER_TRACE
      - Optional

    * - CPPTP_CI_MERGE_REQUEST_CHANGED_FILES
      - Mandatory

**TRIGGERS**
    Job will be triggered only on **merge requests** and only if there were changes in the below files:

    - ``**/*.py``.

check:shell:format
^^^^^^^^^^^^^^^^^^

This job executes `shfmt <https://github.com/mvdan/sh>`__ *only on changed Shell files* (not diff, but files) which will diagnose and fix style violations, or bugs that can be deduced via static analysis.

Configuration file is located at the root of the repository and has ``.editorconfig`` name.

**ARTIFACTS**
    Job will generate a patch file with corrected lines.

**ENVIRONMENT**

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity

    * - CPPTP_CI_LINTER_TRACE
      - Optional

    * - CPPTP_CI_MERGE_REQUEST_CHANGED_FILES
      - Mandatory

**TRIGGERS**
    Job will be triggered only on **merge requests** and only if there were changes in the below files:

    - ``**/*.sh``.

check:shell:lint
^^^^^^^^^^^^^^^^

This job executes `shellcheck <https://github.com/koalaman/shellcheck>`__ *only on changed Shell files* (not diff, but files) which will diagnose and fix style violations like wrong variable and function names.

Configuration file is located at the root of the repository and has ``.shellcheck`` name.

**ARTIFACTS**
    Job will generate a simple log file with all found issues.

**ENVIRONMENT**

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity

    * - CPPTP_CI_LINTER_TRACE
      - Optional

    * - CPPTP_CI_MERGE_REQUEST_CHANGED_FILES
      - Mandatory

**TRIGGERS**
    Job will be triggered only on **merge requests** and only if there were changes in the below files:

    - ``**/*.sh``.

check:yaml:format
^^^^^^^^^^^^^^^^^

This job executes `yamllint <https://github.com/adrienverge/yamllint>`__ *only on changed YAML files* (not diff, but files) which will diagnose and fix style violations.

Configuration file is located at the root of the repository and has ``.yamllint`` name.

**ARTIFACTS**
    Job will generate a simple log file with all found issues.

**ENVIRONMENT**

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity

    * - CPPTP_CI_LINTER_TRACE
      - Optional

    * - CPPTP_CI_MERGE_REQUEST_CHANGED_FILES
      - Mandatory

**TRIGGERS**
    Job will be triggered only on **merge requests** and only if there were changes in the below files:

    - ``**/*.yml``.

Report stage
------------

*See also report stage overview from* :ref:`overview-report-stage`.

report:cpp:coverage
^^^^^^^^^^^^^^^^^^^

**ARTIFACTS**
    Job produces a statically generated HTML reports using `Gcovr <https://github.com/gcovr/gcovr>`__.

**DEPENDENCIES**
    Jobs require artifacts from :ref:`linters-pipeline-check-cpp-coverage` job.

**TRIGGERS**
    Jobs are triggered only on **merge requests** and only if :ref:`linters-pipeline-check-cpp-coverage` job has failed.

report:cpp:cppcheck
^^^^^^^^^^^^^^^^^^^

**ARTIFACTS**
    Job produces a statically generated HTML reports using ``cppcheck-htmlreport`` from the `cppcheck <https://github.com/danmar/cppcheck>`__ package.

**DEPENDENCIES**
    Jobs require artifacts from :ref:`linters-pipeline-check-cpp-cppcheck` job.

**TRIGGERS**
    Jobs are triggered only on **merge requests** and only if :ref:`linters-pipeline-check-cpp-cppcheck` job has failed.

Distribution
============

+--------------------------+---------------------------------------------------------+
| Job                      | System Configuration                                    |
+                          +----------+----------+----------+--------------+---------+
|                          | Alpine 3 | CentOS 7 | CentOS 8 | Ubuntu 20.04 | Windows |
+==========================+==========+==========+==========+==============+=========+
|                          | .. centered:: **Check stage**                           |
+--------------------------+----------+----------+----------+--------------+---------+
| check:cmake:format       | **yes**  | no       | no       | no           | no      |
+--------------------------+----------+----------+----------+--------------+---------+
| check:cmake:lint         | **yes**  | no       | no       | no           | no      |
+--------------------------+----------+----------+----------+--------------+---------+
| check:code:license       | **yes**  | no       | no       | no           | no      |
+--------------------------+----------+----------+----------+--------------+---------+
| check:code:spelling      | **yes**  | no       | no       | no           | no      |
+--------------------------+----------+----------+----------+--------------+---------+
| check:commits:spelling   | **yes**  | no       | no       | no           | no      |
+--------------------------+----------+----------+----------+--------------+---------+
| check:cpp:coverage       | no       | no       | no       | **yes**      | no      |
+--------------------------+----------+----------+----------+--------------+---------+
| check:cpp:format         | Uses its own docker image: ``clang-format``.            |
+--------------------------+----------+----------+----------+--------------+---------+
| check:cpp:include_guards | **yes**  | no       | no       | no           | no      |
+--------------------------+----------+----------+----------+--------------+---------+
| check:cpp:cppcheck       | no       | no       | no       | **yes**      | no      |
+--------------------------+----------+----------+----------+--------------+---------+
| check:cpp:tidy           | no       | no       | no       | **yes**      | no      |
+--------------------------+----------+----------+----------+--------------+---------+
| check:cpp:tidy (nightly) | no       | no       | no       | **yes**      | no      |
+--------------------------+----------+----------+----------+--------------+---------+
| check:docker:lint        | Uses its own docker image: ``hadolint``.                |
+--------------------------+----------+----------+----------+--------------+---------+
| check:python:black       | **yes**  | no       | no       | no           | no      |
+--------------------------+----------+----------+----------+--------------+---------+
| check:python:flake8      | **yes**  | no       | no       | no           | no      |
+--------------------------+----------+----------+----------+--------------+---------+
| check:python:isort       | **yes**  | no       | no       | no           | no      |
+--------------------------+----------+----------+----------+--------------+---------+
| check:shell:format       | **yes**  | no       | no       | no           | no      |
+--------------------------+----------+----------+----------+--------------+---------+
| check:shell:lint         | **yes**  | no       | no       | no           | no      |
+--------------------------+----------+----------+----------+--------------+---------+
| check:yaml:format        | **yes**  | no       | no       | no           | no      |
+--------------------------+----------+----------+----------+--------------+---------+
|                          | .. centered:: **Report stage**                          |
+--------------------------+----------+----------+----------+--------------+---------+
| report:cpp:coverage      | **yes**  | no       | no       | no           | no      |
+--------------------------+----------+----------+----------+--------------+---------+
| report:cpp:cppcheck      | **yes**  | no       | no       | no           | no      |
+--------------------------+----------+----------+----------+--------------+---------+

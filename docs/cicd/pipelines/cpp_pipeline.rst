============
C++ Pipeline
============

This is a pipeline for testing new changes that have been made to anything that is directly related to the C++ part of the project.
Here we try to build project from a clean state on different types of OS and with different set of build configurations to cover as much as possible.
Additionally to this we try to build project with all available sanitizers to detect different kind of issues as fast as possible.

All jobs from C++ pipeline stages are triggered only on **merge requests** and only if changes have been made to C++ files.
Right now such files are detected by these patterns:

- ``**/CMakeLists.txt``,
- ``**/*.cmake``,
- ``**/*.cpp``,
- ``**/*.hpp``,
- ``**/*.ipp``.

.. note:: Please take into account that we don't include jobs from **check** stage, as they are not directly related to this pipeline.

Flowchart
=========

..
  Flowchart has been generated with the help of https://app.diagrams.net/.
  If you need to change flowchart:
    1. Go to the mentioned site.
    2. Import _static/cicd/cpp_pipeline_flowchart.drawio.
    3. Change flowchart
    4. Save as .drawio and .jpg files.
    5. Update appropriate files in _static folder.

.. image:: /_static/cicd/cpp_pipeline_flowchart.jpg
   :target: ../_images/cicd/cpp_pipeline_flowchart.jpg
   :alt: C++ Pipeline Flowchart

Stages overview
===============

Build stage
-----------

*See also test stage overview from* :ref:`overview-build-stage`.

**ALLOW FAILURE**
    Jobs are not allowed to fail as any failure means project can't be fully assembled into distinct binary and library files.

**ARTIFACTS**
    Jobs generate C++ binaries for application executables and tests runners.

    Job artifacts also include all required CMake and CTest files for running tests.

**DEPENDENCIES**
    Jobs don't require artifacts from other jobs.

    Jobs have no dependencies from other stages or jobs.

**IMAGES**
    All jobs from this stage must use pre-build docker images to decrease overall pipeline time.

Test stage
----------

This is a stage for running all available C++ tests.

*See also test stage overview from* :ref:`overview-test-stage`.

Report stage
------------

This is a stage to collect all available build information during the *build* stage and deploy it as a job artifacts if build has failed by any reason.

*See also report stage overview from* :ref:`overview-report-stage`.

Distribution
============

As we try to test as much configurations as possible, in this table all current setups will be tracked.

.. note::
    Those options, that have been stress emphasized using *italic* and marked as (**yes**\*), are not taking part in a build process right now.
    They are put here only to keep a track!

+------------+-----------------------------------------------------------------------+
| Options    | System Configuration                                                  |
+            +-------------------+-------------------+---------------------+---------+
|            | CentOS 7          | CentOS 8          | Ubuntu 20.04        | Windows |
+            +---------+---------+---------+---------+---------+-----------+---------+
|            | Clang   | GCC     | Clang   | GCC     | Clang   | GCC       | MSBuild |
+============+=========+=========+=========+=========+=========+===========+=========+
|            | .. centered:: **Build Configuration**                                 |
+------------+---------+---------+---------+---------+---------+-----------+---------+
| Type       | debug   | release | debug   | debug   | debug   | debug     | debug   |
+------------+---------+---------+---------+---------+---------+-----------+---------+
| Asserts    | **yes** | no      | **yes** | **yes** | **yes** | **yes**   | **yes** |
+------------+---------+---------+---------+---------+---------+-----------+---------+
| Cache      | default | default | default | default | default | default   | default |
+------------+---------+---------+---------+---------+---------+-----------+---------+
| Linter     | default | default | default | default | default | default   | default |
+------------+---------+---------+---------+---------+---------+-----------+---------+
| LTO        | no      | **yes** | no      | **yes** | no      | **yes**   | **yes** |
+------------+---------+---------+---------+---------+---------+-----------+---------+
| VCS Info   | no      | **yes** | no      | no      | no      | no        | no      |
+------------+---------+---------+---------+---------+---------+-----------+---------+
|            | .. centered:: **Binding**                                             |
+------------+---------+---------+---------+---------+---------+-----------+---------+
| Batch      | no      | no      | no      | no      | no      | no        | no      |
+------------+---------+---------+---------+---------+---------+-----------+---------+
| JS         | no      | no      | no      | no      | no      | no        | no      |
+------------+---------+---------+---------+---------+---------+-----------+---------+
| Lua        | no      | no      | no      | no      | no      | no        | no      |
+------------+---------+---------+---------+---------+---------+-----------+---------+
| Python     | no      | no      | no      | no      | no      | no        | no      |
+------------+---------+---------+---------+---------+---------+-----------+---------+
|            | .. centered:: **Docs**                                                |
+------------+---------+---------+---------+---------+---------+-----------+---------+
|            | no      | no      | no      | no      | **yes** | **yes**\* | no      |
+------------+---------+---------+---------+---------+---------+-----------+---------+
|            | .. centered:: **Linters**                                             |
+------------+---------+---------+---------+---------+---------+-----------+---------+
| *Coverage* | no      | no      | no      | no      | no      | **yes**\* | no      |
+------------+---------+---------+---------+---------+---------+-----------+---------+
| *CppCheck* | no      | no      | no      | no      | no      | **yes**\* | no      |
+------------+---------+---------+---------+---------+---------+-----------+---------+
| IWYU       | no      | no      | no      | no      | no      | no        | no      |
+------------+---------+---------+---------+---------+---------+-----------+---------+
| LWYU       | **yes** | **yes** | **yes** | **yes** | **yes** | **yes**   | **yes** |
+------------+---------+---------+---------+---------+---------+-----------+---------+
|            | .. centered:: **Sanitizers**                                          |
+------------+---------+---------+---------+---------+---------+-----------+---------+
| ASan       | no      | no      | no      | **yes** | no      | no        | no      |
+------------+---------+---------+---------+---------+---------+-----------+---------+
| LSan       | no      | no      | no      | **yes** | no      | no        | no      |
+------------+---------+---------+---------+---------+---------+-----------+---------+
| MSan       | no      | no      | no      | no      | no      | no        | no      |
+------------+---------+---------+---------+---------+---------+-----------+---------+
| UBSan      | no      | no      | no      | **yes** | no      | no        | no      |
+------------+---------+---------+---------+---------+---------+-----------+---------+
| TSan       | no      | no      | no      | no      | **yes** | no        | no      |
+------------+---------+---------+---------+---------+---------+-----------+---------+
| Valgrind   | no      | no      | no      | no      | no      | **yes**   | no      |
+------------+---------+---------+---------+---------+---------+-----------+---------+
|            | .. centered:: **Tests**                                               |
+------------+---------+---------+---------+---------+---------+-----------+---------+
| Benchmarks | **yes** | **yes** | **yes** | **yes** | **yes** | **yes**   | **yes** |
+------------+---------+---------+---------+---------+---------+-----------+---------+
| Tests      | **yes** | **yes** | **yes** | **yes** | **yes** | **yes**   | **yes** |
+------------+---------+---------+---------+---------+---------+-----------+---------+

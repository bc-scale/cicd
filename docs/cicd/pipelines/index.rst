#########
Pipelines
#########

.. toctree::
   :maxdepth: 1
   :hidden:

   cpp_pipeline.rst
   docker_pipeline.rst
   docs_pipeline.rst
   linters_pipeline.rst

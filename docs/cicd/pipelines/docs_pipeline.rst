=============
Docs Pipeline
=============

This is a pipeline for testing and deploying new changes that have been made to anything that is directly related to the *documentation* part of the project.

All jobs from documentation pipeline stages are triggered only if changes have been made to documentation related files.
Right now such files are detected by these patterns:

- ``docs/**/*``
- ``project/**/CMakeLists.txt``
- ``project/**/include/**/*.hpp``

As you can see from the above list of files pipeline will take into account both types of the project documentation: Doxygen (C++ files) and Sphinx (RST files) documentation.

Flowchart
=========

This is a very generalized flowchart that combines in itself all possible roots of pipeline generation and execution.

..
  Flowchart has been generated with the help of https://app.diagrams.net/.
  If you need to change flowchart:
    1. Go to the mentioned site.
    2. Import _static/cicd/docs_pipeline_flowchart.drawio.
    3. Change flowchart
    4. Save as .drawio and .jpg files.
    5. Update appropriate files in _static folder.

.. image:: /_static/cicd/docs_pipeline_flowchart.jpg
   :target: ../_images/cicd/docs_pipeline_flowchart.jpg
   :alt: Docs Pipeline Flowchart

Stages overview
===============

Build stage
-----------

This stage is responsible for building HTML documentation from a clean state to make sure there are no unexpected results.

**ALLOW FAILURE**
    Jobs are not allowed to fail as any failure here means fundamental problems in documentation generation.

**ARTIFACTS**
    Jobs generate an artifact with a HTML documentation.

**DEPENDENCIES**
    Jobs don't require artifacts from other jobs.

    Jobs has no dependencies from other stages or jobs.

**IMAGES**
    All jobs from this stage must use one of pre-build docker images to decrease overall pipeline time.

**TRIGGERS**
    Jobs are triggered only on **merge requests**.

Deploy stage
------------

This stage is responsible for building and deploying static HTML documentation files to the project registry.

**ALLOW FAILURE**
    Jobs are not allowed to fail as these jobs are run only on ``CI_DEFAULT_BRANCH`` after accepting a successful merge request!

**ARTIFACTS**
    New version of the documentation will be deployed to the project `generic packages repository <https://docs.gitlab.com/ee/user/packages/generic_packages>`__ in a form an ``tar`` archive.

**ENVIRONMENT**

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity

    * - CPPTP_CI_DEPLOY_TOKEN
      - Mandatory

    * - CPPTP_CI_DOCS_NAME
      - Mandatory

    * - CPPTP\_CI\_\*_DOCS_NAME
      - Mandatory

    * - CPPTP_CI_GENERIC_PACKAGES_REGISTRY
      - Mandatory

**DEPENDENCIES**
    Jobs don't require artifacts from other jobs.

    Jobs has no dependencies from other stages or jobs.

**IMAGES**
    Nothing specific here.

**TRIGGERS**
    Jobs are triggered only on ``CI_DEFAULT_BRANCH``.

Misc stage
----------

After a successful deploy, job from this stage will trigger a pipeline of `other project <https://gitlab.com/cpptp/cpptp.gitlab.io>`__ using `GitLab API <https://docs.gitlab.com/ee/api/pipeline_triggers.html>`__.
If external pipeline succeed, new version of documentation will be available on https://cpptp.gitlab.io.

**ALLOW FAILURE**
    Jobs are not allowed to fail as these jobs are run only on ``CI_DEFAULT_BRANCH`` after accepting a successful merge request!

**ARTIFACTS**
    Jobs don't generate any artifacts.

**DEPENDENCIES**
    Jobs don't require artifacts from other jobs.

    Jobs has a dependency to a job from *deploy* stage.

**IMAGES**
    No image is required.

**TRIGGERS**
    Jobs are triggered only on ``CI_DEFAULT_BRANCH``.

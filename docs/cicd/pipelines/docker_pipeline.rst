===============
Docker Pipeline
===============

This is a pipeline for testing and deploying new changes that have been made to anything that is directly related to the *Docker* part of the project.

All jobs from docker pipeline stages are triggered only if changes have been made to Docker related files.
Right now such files are detected by these patters:

- ``cmake/Conan.cmake``,
- ``docker/*.Dockerfile``,
- ``scripts/os/setup_*.sh``,
- ``scripts/fetch_conan_dependencies.sh``
- ``requirements.txt``.

Overview
========

Build stage
-----------

This stage is responsible for building Docker images from a clean state to make sure there are no unexpected results.

**ALLOW FAILURE**
    Jobs are not allowed to fail as any failure here means fundamental problems in a specific docker image.

**ARTIFACTS**
    Jobs don't generate any artifacts.

**DEPENDENCIES**
    Jobs don't require artifacts from other jobs.

    Jobs has no dependencies from other stages or jobs.

**IMAGES**
    Nothing specific here.

**TRIGGERS**
    Jobs are triggered only on **merge requests**.

Deploy stage
------------

This stage is responsible for generated pre-build docker images for our pipelines.
So called pre-build images are crucial part of our project CI/CD as it allows us to decrease execution time of jobs.

**ALLOW FAILURE**
    Jobs are not allowed to fail as these jobs are run only on ``CI_DEFAULT_BRANCH`` after accepting a successful merge request!

**ARTIFACTS**
    New docker images will be deployed to the project `container registry <https://docs.gitlab.com/ee/user/packages/container_registry/>`__.

**DEPENDENCIES**
    Jobs don't require artifacts from other jobs.

    Jobs has no dependencies from other stages or jobs.

**IMAGES**
    Nothing specific here.

**TRIGGERS**
    Jobs are triggered only on ``CI_DEFAULT_BRANCH``.

###################
CI/CD Documentation
###################

.. toctree::
   :maxdepth: 1
   :hidden:

   overview.rst
   misc_jobs.rst
   sub_pipelines.rst
   pipelines/index.rst
   variables.rst

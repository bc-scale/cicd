=============
Sub-Pipelines
=============

We always try to compose everything into one top-level `GitLab CI/CD pipeline <https://docs.gitlab.com/ee/ci/pipelines/>`__ that includes all jobs.
This way we can have all `GitLab CI/CD jobs <https://docs.gitlab.com/ee/ci/jobs/>`__, that have been triggered for merge request or a specific branch, in one place.

That said, we still try to decompose a top-level pipeline into smaller ones ("sub-pipelines").
Such approach decrease cognitive overload and makes it much easy to understand certain aspects of our pipeline.

Below table ("map") shows how jobs are distributed across different sub-pipelines.

..
  Flowchart has been generated with the help of https://app.diagrams.net/.
  If you need to change flowchart:
    1. Go to the mentioned site.
    2. Import _static/cicd/jobs_map.drawio.
    3. Change flowchart
    4. Save as .drawio and .jpg files.
    5. Update appropriate files in _static folder.

.. image:: /_static/cicd/jobs_map.jpg
   :target: ../_images/cicd/jobs_map.jpg
   :alt: Map of Sub-Pipelines Jobs

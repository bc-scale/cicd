========
Overview
========

This page is geared towards developers of the project CI/CD pipelines, and serves as a starting point of understanding project pipelines.

General workflow
================

A below table shows when jobs of a certain stage will be triggered.

+--------+---------+-------------------+---------------+
| Stage  | Branch  | CI_DEFAULT_BRANCH | Merge request |
+========+=========+===================+===============+
| build  | no      | no                | **yes**       |
+--------+---------+-------------------+---------------+
| test   | no      | no                | **yes**       |
+--------+---------+-------------------+---------------+
| check  | no      | no                | **yes**       |
+--------+---------+-------------------+---------------+
| report | no      | no                | **yes**       |
+--------+---------+-------------------+---------------+
| deploy | no      | **yes**           | no            |
+--------+---------+-------------------+---------------+
| misc   | no      | **yes**           | no            |
+--------+---------+-------------------+---------------+

Stages overview
===============

Before you start reading a below overview of each pipeline stage, you should familiarize yourself with section descriptions.

.. list-table::
   :header-rows: 1
   :widths: 10 50

   * - Section
     - Description

   * - Allow failure
     - This section describes whether jobs are allowed to fail.

   * - Artifacts
     - This section describes what `job artifacts <https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html>`__ will be generated after a successful run.

   * - Dependencies
     - This section describes what kind of dependencies tends to have.
       Effectively combines `dependencies <https://docs.gitlab.com/ee/ci/yaml/#dependencies>`__ and `needs <https://docs.gitlab.com/ee/ci/yaml/#needs>`__ directives.

   * - Environment
     - This section describes what `environment variables <https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project>`__ jobs use.
       Right now only custom variables, those ones that defined using project settings, are taken into account.

   * - Images
     - This section describes Docker images that the jobs should run in.
       Effectively combines an overall content of  `image <https://docs.gitlab.com/ee/ci/yaml/#image>`__ directive for all jobs.

   * - Triggers
     - This section describes when at what cases jobs will be triggered.

.. _overview-build-stage:

Build stage
-----------

This is an *optional* stage for testing new changes that have been made to different parts of the project.
Especially this goes for C++ and Docker files, as these parts require assembling of mentions parts.

**ALLOW FAILURE**
    Jobs are not allowed to fail as any failure means fundamental problems in one of the project parts.

**ARTIFACTS**
    Jobs generate C++ binaries for application executables and tests runners.

    Job artifacts also include all required CMake and CTest files for running tests.

    .. note:: Please take into account that generation of useful human-readable reports are usually delegated to jobs of the ``report`` stage.

**DEPENDENCIES**
    Jobs don't require artifacts from other jobs.

    Jobs has no dependencies from other stages or jobs.

**IMAGES**
    If possible, all jobs must use pre-build docker images to decrease overall pipeline time.

**TRIGGERS**
    Jobs are triggered only on **merge requests** and if a certain part of the projects have been modified (this can be C++ or Docker files).

.. _overview-test-stage:

Test stage
----------

This is an *optional* stage for running different types of project tests.

**ALLOW FAILURE**
    Jobs are not allowed to fail as all project tests regardless their type must never be broken.

**ARTIFACTS**
    Jobs will generate `unit test reports <https://docs.gitlab.com/ee/ci/unit_test_reports.html>`__.

**DEPENDENCIES**
    Jobs require artifacts from parent jobs.
    Those artifacts will contain test runners which will be executed.

**IMAGES**
    All jobs must use the same pre-build docker image which has been used by their parent jobs.

**TRIGGERS**
    Jobs are triggered only on **merge requests** and only if parent job from the **build** stage hasn't failed.

.. _overview-check-stage:

Check stage
-----------

This is an *optional* stage for executing different linters to diagnose code for violation of the project conventions and to detect different defects and issues.

**ALLOW FAILURE**
    Jobs are not allowed to fail as all project code must be follow established conventions and contain no know defects and issues.

**ARTIFACTS**
    Jobs may generate useful human-readable reports by themselves or save raw results and delegate reports generation to jobs of the ``report`` stage.

**DEPENDENCIES**
    Jobs don't require artifacts from other jobs.

    Jobs has no dependencies from other stages or jobs.

**IMAGES**
    All jobs are on their own to decide what docker image to use for their execution.

**TRIGGERS**
    Jobs are triggered only on **merge requests** and if a certain part of the projects have been modified (this can be C++, Docker, Shell files and so on...).

.. _overview-report-stage:

Report stage
------------

This is an *optional* utility stage for generating reports for parent jobs in from stages.

**ALLOW FAILURE**
    This jobs are tend to never fail, but generally speaking they might sometimes...

**ARTIFACTS**
    Depending on a type of a parent job and its artifacts, generated artifacts can vary from simple text files to a statically generated HTML reports.

**DEPENDENCIES**
    Jobs require artifacts from parent jobs.
    Those artifacts will contain useful data for generating reports.

**IMAGES**
    All jobs must use the smallest docker image that is currently available.
    For now this is a pure ``Alpine`` image.

**TRIGGERS**
    Jobs are triggered only on **merge requests** and only if parent job has failed.

.. _overview-deploy-stage:

Deploy stage
------------

This is an *optional* stage for deploying different packages to a project or group `registry`_.

**ALLOW FAILURE**
    These jobs must never fail, as they are run only on ``CI_DEFAULT_BRANCH`` after accepting a successful merge request!

**ARTIFACTS**
    Jobs don't generate any jobs artifacts.
    Instead of that they may deploy packages to a `registry`_ of this project, other projects or a parent group.

**DEPENDENCIES**
    Jobs don't require artifacts from other jobs.

    Jobs has no dependencies from other stages or jobs.

**IMAGES**
    All jobs are on their own to decide what docker image to use for their execution.

**TRIGGERS**
    Jobs are triggered only on commits to ``CI_DEFAULT_BRANCH``.

.. _registry: https://docs.gitlab.com/ee/user/packages/index.html

.. _overview-misc-stage:

Misc stage
----------

This is an *optional* stage for different miscellaneous stuff.

**ALLOW FAILURE**
    These jobs must never fail, as they are run only on ``CI_DEFAULT_BRANCH``!

**ARTIFACTS**
    Jobs don't generate any jobs artifacts.

**DEPENDENCIES**
    Jobs don't require artifacts from other jobs.

    Jobs still might depend on other stages or jobs.

**IMAGES**
    All jobs are on their own to decide what docker image to use for their execution.

**TRIGGERS**
    Jobs are triggered only by other stages/jobs or by a scheduler.

##########
Git Guides
##########

.. toctree::
   :maxdepth: 1
   :hidden:

   Configuration & Recommendations <https://cpptp.gitlab.io/git/setup.html>
   Repository <https://cpptp.gitlab.io/git/repo.html>
   Commit Message Conventions <https://cpptp.gitlab.io/git/commit_message.html>
   Commit Organization Best Practices <https://cpptp.gitlab.io/git/best_practices.html>

   tags.rst

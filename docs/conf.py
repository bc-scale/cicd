# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

import datetime
import os

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))

# -- Project information -----------------------------------------------------


project = "Cpp Template Projecte"
author = "Cpp Template Projecte Developers"

this_year = datetime.datetime.now().year

copyright = f"2020-{this_year}, Zinchenko Serhii"  # noqa: A001

# The full version, including alpha/beta/rc tags
# release = 'https://gitlab.com/pages/sphinx'

# Tell sphinx what the pygments highlight language should be.
highlight_language = "cpp"

# The language for content autogenerated by Sphinx. Refer to documentation
# for a list of supported languages.
#
# This is also used if you do content translation via gettext catalogs.
# Usually you set "language" from the command line for these cases.
language = "en"

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = ["sphinx.ext.ifconfig", "sphinx.ext.mathjax", "sphinx.ext.todo"]

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]

# The suffix(es) of source filenames.
# You can specify multiple suffix as a list of string:
# source_suffix = ['.rst', '.md']
source_suffix = ".rst"

# Tell sphinx what the primary language being documented is.
# primary_domain = 'cpp'

# -- Options for HTML output -------------------------------------------------


def generate_last_updated_fmt():
    if not os.getenv("CI", False):
        return ""

    branch = os.environ["CI_COMMIT_REF_NAME"]
    commit_sha = os.environ["CI_COMMIT_SHORT_SHA"]

    return f"{commit_sha} ({branch}), %y-%m-%d %H:%M:%S"


# If this is not the empty string, a ‘Last updated on:’ timestamp is inserted
# at every page bottom, using the given strftime() format.
html_last_updated_fmt = generate_last_updated_fmt()

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "furo"

# Title of the docs
html_title = project

# Add links to the reST sources to the sidebar.
html_show_sourcelink = False

# Add “Created using Sphinx” to the HTML footer.
html_show_sphinx = True

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
# html_static_path = ['_static']

#
# Graphs
#

graphviz_output_format = "svg"

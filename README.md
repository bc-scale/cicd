# Cpp Template Projecte - Common CI/CD Tools

This repository contains content for our CI/CD.

## Contributing

1. Clone this repository:

```bash
$ mkdir cpptp
$ cd cpptp
$ git clone https://gitlab.com/cpptp/cicd.git
$ cd cicd
```

You can also clone this repository using [GitLab token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html).

```bash
$ git clone https://oauth2:<ACCESS_TOKEN>@gitlab.com/cpptp/cicd.git
```

*For alternative ways you can check out [this SO thread](https://stackoverflow.com/questions/25409700).*

2. Setup a default merge strategy for pulling branches.

```bash
$ git config pull.rebase true
```

3. Create a branch.
   This can either be an informal user branch or a full-fledged ticket branch tracked in JIRA.

4. Make and commit your edits.

5. Push your development branch to GitLab and make a merge request.
   The merge request page will help you track the publishing and testing status of your branch.

6. Once you've passed a code review, ask of repository maintainers to merge your changes to `master` branch.

## Prepearing development environment

*Assuming you are using of Ubuntu based distributions.*

### Installing your favorite code editor

Please consult our [documentation](https://cpptp.gitlab.io/editors/index.html) for configuration suggestions of your favorite code editor.

## Editing entirely on GitLab

If you're in a hurry, you don't need to worry about cloning the Developer Guide; you can do everything on GitLab.com.
See [GitLab's documentation](https://docs.gitlab.com/ee/user/project/web_ide/) on editing files and creating branches entirely from [GitLab.com](https://gitlab.com).

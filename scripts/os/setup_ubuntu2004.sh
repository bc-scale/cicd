#!/usr/bin/env bash

# Copyright 2020-2022 Zinchenko Serhii <zinchenko.serhii@pm.me>.
#
# Cpp Template Project: A template CMake project to get you started with
# C++ and tooling.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# DESCRIPTION:
#
# This script is mainly supposed to be used only by Ubuntu 20.04 docker image to
# provide a pre-build image with all pre-installed and pre-configured system
# packages and project dependencies.

set -e

#------------------------------------------------------------------------------#
# Packages                                                                     #
#------------------------------------------------------------------------------#

declare -a INSTALL_PKGS=(
    "clang-12"
    "cmake"
    "cppcheck"
    "curl"
    "doxygen"
    "g++-10"
    "gcc-10"
    "git"
    "graphviz"
    "jq"
    "libpython3.8-dev" # pybind11
    "lcov"
    "llvm-12" # symbolizer for sanitizers
    "make"
    "python3-pip"
    "valgrind"
    "wget"
)

# Install

# set noninteractive installation
export DEBIAN_FRONTEND=noninteractive
export TZ=Europe/Kiev

apt-get update
apt-get install --no-install-recommends -y "${INSTALL_PKGS[@]}"

# Install clang-*-14

apt-get install --no-install-recommends -y gnupg

# see https://apt.llvm.org/.
echo "deb http://apt.llvm.org/focal/ llvm-toolchain-focal-14 main" >> /etc/apt/sources.list
echo "deb-src http://apt.llvm.org/focal/ llvm-toolchain-focal-14 main" >> /etc/apt/sources.list

wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add -
apt-get update
apt-get install --no-install-recommends -y clang-tidy-14

# Clean

apt-get clean
rm -rf /var/lib/apt/lists/*

#------------------------------------------------------------------------------#
# Packages Aliases                                                             #
#------------------------------------------------------------------------------#

# See https://stackoverflow.com/questions/7832892
# See https://stackoverflow.com/questions/67298443

# Clang

update-alternatives \
    --install /usr/bin/clang clang /usr/bin/clang-12 100 \
    --slave /usr/bin/clang++ clang++ /usr/bin/clang++-12

# GCC

update-alternatives --install /usr/bin/cpp cpp /usr/bin/cpp-10 110

update-alternatives \
    --install /usr/bin/gcc gcc /usr/bin/gcc-10 110 \
    --slave /usr/bin/g++ g++ /usr/bin/g++-10 \
    --slave /usr/bin/gcov gcov /usr/bin/gcov-10 \
    --slave /usr/bin/gcc-ar gcc-ar /usr/bin/gcc-ar-10 \
    --slave /usr/bin/gcc-ranlib gcc-ranlib /usr/bin/gcc-ranlib-10

#------------------------------------------------------------------------------#
# Python                                                                       #
#------------------------------------------------------------------------------#

pip3 install --no-cache-dir -r 'requirements.txt'

#!/usr/bin/env bash

# Copyright 2022 Zinchenko Serhii <zinchenko.serhii@pm.me>.
#
# Cpp Template Project: A template CMake project to get you started with
# C++ and tooling.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# DESCRIPTION:
#
# Extract all commits from HEAD of current branch to its parent and check them
# for typos.
#
# ATTENTION:
#
# This script is mainly supposed to be used only by CI/CD linter jobs.

set -e

declare -r CODESPELL_LOG_FILE="codespell.txt"
declare -r COMMIT_MSG_FILE="commit.txt"

declare -r LOG_FILE="codespell.log"

declare -r DEFAULT_SETTINGS_FILE=".codespellrc"
if [[ -s "${DEFAULT_SETTINGS_FILE}" ]]; then
    declare -r SETTINGS_FILE="${DEFAULT_SETTINGS_FILE}"
elif [[ -n "${CPPTP_CODESPELL_CFG}" ]]; then
    declare -r SETTINGS_FILE="${CPPTP_CODESPELL_CFG}"
fi

#######################################
# Extract all commits from HEAD of current branch to its parent and check them
# for typos.
# Globals:
#   CODESPELL_LOG_FILE
#   COMMIT_MSG_FILE
#   LOG_FILE
#   SETTINGS_FILE
# Arguments:
#   $1 - target Git branch
# Outputs:
#   Writes all found typos to stdout and to a log file.
#######################################
check()
{
    local -r target_branch="${1}"

    git fetch origin "${target_branch}"

    # Redirect everything to log file.
    # https://stackoverflow.com/questions/11229385
    exec 3>&1 1> "${LOG_FILE}"

    for commit in $(git rev-list "origin/${target_branch}..HEAD"); do
        git log -n 1 "${commit}" > "${COMMIT_MSG_FILE}"

        codespell \
            --config "${SETTINGS_FILE}" \
            --enable-colors \
            "${COMMIT_MSG_FILE}" \
            > "${CODESPELL_LOG_FILE}" || true

        if [[ -s "${CODESPELL_LOG_FILE}" ]]; then
            cat "${COMMIT_MSG_FILE}"
            echo ""
            cat "${CODESPELL_LOG_FILE}"
            echo ""
        fi
    done

    # Restore output to terminal.
    # https://stackoverflow.com/questions/25474854
    exec 1>&3 3>&-

    # Show our log in terminal.
    # https://unix.stackexchange.com/questions/38634
    less -SEX -R codespell.log

    # Removing colors from output
    # https://stackoverflow.com/a/18000433
    sed -i -r "s/\x1B\[([0-9]{1,3}(;[0-9]{1,2})?)?[mGK]//g" "${LOG_FILE}"

    # Fail if there are some changes
    # shellcheck disable=SC2046
    exit $(grep -c "commit.txt" "${LOG_FILE}")
}

#######################################
# Main entry point for this script.
# Arguments:
#   $1 - a list of files to be checked
#######################################
main()
{
    if [[ -n "${1}" ]]; then
        check "${1}"
    elif [[ -n "${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}" ]]; then
        check "${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}"
    else
        echo -e "\033[0;31mFailed to detect Git target branch!\033[0m" >&2
        exit 1
    fi
}

main "$@"

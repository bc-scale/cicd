#!/usr/bin/env bash

# Copyright 2021-2022 Zinchenko Serhii <zinchenko.serhii@pm.me>.
#
# Cpp Template Project: A template CMake project to get you started with
# C++ and tooling.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# DESCRIPTION:
#
# Fetch Conan dependencies for a all compilers and build types supported by
# project.
#
# ATTENTION:
#
# This script is mainly supposed to be used only by docker images to provide
# a pre-build images with all pre-installed system packages and project
# dependencies.

set -e

#######################################
# Fetch Conan dependencies for a specific set of compilers and build type.
# Arguments:
#   $1 - C compiler
#   $2 - C++ compiler
#   $3 - CMake build type
#######################################
fetch()
{
    local -r cc="${1}"
    local -r cxx="${2}"
    local -r type="${3}"

    echo -e "\033[0;33mFetching Conan dependencies for CC=${cc}, CXX=${cxx}, CMAKE_BUILD_TYPE=${type} configuration...\033[0m"

    CC="${cc}" CXX="${cxx}" cmake -S. -Bbuild -DFETCH_CONAN_DEPENDENCIES=1 -DCMAKE_BUILD_TYPE="${type}"

    # 2. Clean all build data, otherwise conan will ignore new CC and CXX variables.
    rm -rf build

    # 3. Clean-up conan cache
    # https://stackoverflow.com/questions/50247671
    conan remove "*" -s -f
}

#######################################
# Main entry point for this script.
# Arguments:
#   None
#######################################
main()
{
    fetch 'clang' 'clang++' 'Debug'
    fetch 'clang' 'clang++' 'Release'
    fetch 'gcc' 'g++' 'Debug'
    fetch 'gcc' 'g++' 'Release'
}

main
